# <div align="center">Sander Schoone</div>
#### <div align="center">Software developer at Schooltoday b.v.</div>
<div align="center">[GitLab: @s4nderdevelopment](https://gitlab.com/s4nderdevelopment)<br/>[GitHub: @S4nderDevelopment](https://github.com/S4nderDevelopment)</div>

Welcome! My name is Sander Schoone. I work as a software developer at Schooltoday b.v., primarily developing and maintaining [MijnKleutergroep](https://mijnkleutergroep.nl).

> <small>
>     MijnKleutergroep is a student tracking system, primarily for teachers with the first 2 or 3 year classes of elementary school (in Dutch: basisschool, kleuters).
> </small>

In my free time I work on open source hobby projects hosted here on GitLab.

## <div align="center">Pinned projects</div>

<table style="width: 100%;">
    <thead>
        <tr width="100%">
            <th align="center">
                <p>Linux/Windows Setup</p>
                <small>[Link (Linux)](https://gitlab.com/s4nderdevelopment/linux-setup)</small>
                <br/>
                <small>[Link (Windows)](https://gitlab.com/s4nderdevelopment/windows-setup)</small>
            </th>
            <th align="center">
                <p>OpenSSL certificate example</p>
                <small>[Link](https://gitlab.com/s4nderdevelopment/ca-cert-example-openssl)</small>
            </th>
            <th align="center">
                <p>SFX Controlbox</p>
                <small>[Link](https://gitlab.com/s4nderdevelopment/sfx-control-box-extension)</small>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Default configuration for a new <b>Linux or Windows</b> installation (bare metal or VM) according to my personal preferences.
            </td>
            <td>
                Helper scripts to generate OpenSSL CA/non-CA certificates and keys. Includes a bash script for Linux and a PowerShell script for Windows.
            </td>
            <td>
                Live on the fly tuning of all parameters in SimFeedback like min speed, max speed, acceleration, smoothness and intensity using any button box. Full control of all effects and the ability to save per profile button-configurations.
                <img src="https://gitlab.com/s4nderdevelopment/sfx-control-box-extension/-/raw/master/Screenshot%201.png?ref_type=heads">
            </td>
        </tr>
    </tbody>
</table>


